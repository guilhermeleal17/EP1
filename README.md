# EP1 - Orientacao a Objetos (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

## Neste arquivo deve conter as instruções de execução e descrição do projeto.

EP1 Descrição: 
### Como Compilar e Executar
Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Abra o terminal;
* Encontre o diretório raiz do projeto;
* Limpe os arquivos objeto:
	**$ make clean 
* Compile o programa: 
	**$ make
* Execute:
	**$ make run

Observações:

As imagens devem ter formato .ppm e localizadas na pasta ./doc
A nova imagem com filtro aplicado também será salva na pasta ./doc
NÃO COLOQUE A EXTESÃO DO ARQUIVO .ppm, coloque somente o nome da imagem, 
exemplo: "unb.ppm" -> coloque "unb" quando solicitado e "unb_nomeFiltro".

