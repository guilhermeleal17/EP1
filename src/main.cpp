#include <iostream>
#include <fstream>
#include <string>
#include "filtronegativo.h"
#include "imagem.h"
#include "filtropolarizado.h"
#include "filtroPB.h"

void menu();
int validaFiltro(int valor);
int validaRepeticao(int valor);
using namespace std;


int main(){
	int filtro_requerido=0;
    int a =1;

    while(a!=0){
        menu();
        filtro_requerido = validaFiltro(filtro_requerido);

        system ("clear");
        FiltroNegativo *filtronegativo = new FiltroNegativo();
        FiltroPolarizado *filtropolarizado = new FiltroPolarizado();
        FiltroPB *filtropretobranco = new FiltroPB();

        ofstream imagem_saida;
        switch(filtro_requerido){
            case 1:
                filtronegativo->AbrirImagem(imagem_saida);
                break;

            case 2:
                filtropretobranco->AbrirImagem(imagem_saida);
                break;

            case 3:
                filtropolarizado->AbrirImagem(imagem_saida);
                break;
        }
        cout << "Deseja repetir?   (Digite - 1 para sim / Digite - 0 para não): ";
        a = validaRepeticao(a);
    }
	return 0;
}

void menu(){
    cout << "\nEP1 - 2016 - Orientacao a Objetos" << endl;
	cout << "\n\nEscolha um filtro." << endl;
	cout << "\n1. Negativo \n2. Preto e Branco \n3. Polarizado \nOpcao:";
}

int validaFiltro( int valor){
    cin >> valor;
	while(valor < 1 || valor> 3){
		cout << "\nOpção inválida, escolha uma abaixo: " << endl;
		cout << "\n1. Negativo \n2. Preto e Branco \n3. Polarizado \nOpcao: ";
		cin >> valor;
	}
	return valor;
}

int validaRepeticao(int valor){
    cin>> valor;
    while(valor < 0 || valor> 1){
        cout<< "\nopcao invalida. Tente novamente: ";
        cin>> valor;
    }
    return valor;
}
