#include <iostream>
#include <string>
#include <fstream>
#include <cstring>
#include <cstdio>
#include <vector>
#include "imagem.h"
using namespace std;
Imagem::Imagem(){
	magic_number = "";
	color = 255;
	largura = 0;
	altura = 0;
	matrizR = new unsigned char*[altura];
        for(int i = 0; i < altura; i++){
                matrizR[i] = new unsigned char[largura];
        }
        matrizG = new unsigned char*[altura];
        for(int i = 0; i < altura; i++){
                matrizG[i] = new unsigned char[largura];
        }

        matrizB = new unsigned char*[altura];
        for(int i = 0; i < altura; i++){
                matrizB[i] = new unsigned char[largura];
        }
}
Imagem::Imagem(string magic_number, int largura, int altura, int color){
	this->magic_number = magic_number;
	this->largura = largura;
	this->altura = altura;
	this->color = color;
}
Imagem::~Imagem(){
}

string Imagem::getMagic_number(){
	return magic_number;
}
void Imagem::setMagic_number(string magic_number){
	this->magic_number = magic_number;
}
int Imagem::getColor(){
	return color;
}
void Imagem::setColor(int color){
	this->color = color;
}
int Imagem::getLargura(){
        return largura;
}
void Imagem::setLargura(int largura){
        this->largura = largura;
}
int Imagem::getAltura(){
        return altura;
}
void Imagem::setAltura(int altura){
        this->altura = altura;
}

void Imagem::AbrirImagem(ofstream &imagem_saida){
        ifstream photo;
        string nome_da_foto;

         do{
            cout << "Nome da imagem à ser modificada: " << endl;
            cin >> nome_da_foto;
            nome_da_foto = "./doc/" + nome_da_foto + ".ppm";
            photo.open(nome_da_foto.c_str(), ios::binary);
            if(!photo.is_open()){
                cout << "\nErro na abertura da imagem!\n"<<  endl;
            }
        }while (!photo.is_open());

		string magic_number;
		string hastag;
		int color;
		int largura;
		int altura;
		getline(photo, magic_number);
		if(magic_number != "P6"){
			cout << "Formato de imagem inválido" << endl;
			return;
		}
		while(1){
			getline(photo, hastag);
			if(hastag[0] != '#'){
				cout << "Comentario: " << hastag << endl;
				int size_hastag = hastag.length() + 1;

				cout << "Tamanho: " << size_hastag  << endl;
				photo.seekg(-size_hastag,ios_base::cur);

				photo >> largura;
 				cout << "Largura: " << largura << endl;
 				photo >> altura;
 				cout << "Altura: " << altura << endl;
 				photo >> color;
 				cout << "Intensidade máxima: " << color << endl;
					break;
			}
		}
		setColor(color);
		setLargura(largura);
		setAltura(altura);
		setMagic_number(magic_number);
		photo.seekg(1,ios_base::cur);

		string novo_nome;
		cout << "Novo nome da foto: " << endl;
		cin >> novo_nome;
		novo_nome = "./doc/" + novo_nome + ".ppm";

		imagem_saida.open(novo_nome.c_str(),ios::binary);

		imagem_saida << magic_number << endl;
		imagem_saida << largura << " " << altura <<  endl;
		imagem_saida << color << endl;
		int m;
		int n;
		char red, green, blue;

		matrizR = new unsigned char*[altura];
		for(int j = 0; j < altura; j++){
			matrizR[j] = new unsigned char[largura];
		}

		matrizG = new unsigned char*[altura];
		for(int j = 0; j < altura; j++){
 			matrizG[j] = new unsigned char[largura];
		}

		matrizB = new unsigned char*[altura];
 		for(int j = 0; j < altura; j++){
			matrizB[j] = new unsigned char[largura];
		}

		for(m = 0; m < altura; m++){
			for(n = 0; n < largura; n++){
				photo.get(red);
				photo.get(green);
				photo.get(blue);
				matrizR[m][n] = red;
				matrizG[m][n] = green;
				matrizB[m][n] = blue;
			}
		}

	pixels(imagem_saida);
	photo.close();
	imagem_saida.close();
}
void Imagem::pixels(ofstream &imagem_saida){
 	int **r1, **g1, **b1;

 	r1 = new int*[altura];
 	for(int i = 0; i < altura; i++){
	 	r1[i] = new int[largura];
 	}
 	g1 = new int*[altura];

	for(int l = 0; l < altura; l++){
 		g1[l] = new int[largura];
 	}
 	b1 = new int*[altura];

	for(int k = 0; k < altura; k++){
 		b1[k] = new int[largura];
 	}

 	int p, s;
	for(p = 0; p < altura; p++){
		for(s = 0; s < largura; s++){
			r1[p][s] = (unsigned int)matrizR[p][s];
			imagem_saida << (char)r1[p][s];

			g1[p][s] = (unsigned int)matrizG[p][s];
			imagem_saida << (char)g1[p][s];
			b1[p][s] = (unsigned int)matrizB[p][s];
			imagem_saida << (char)b1[p][s];

		}
	}
}
