#include <iostream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include "filtronegativo.h"
using namespace std;

FiltroNegativo::FiltroNegativo(){
	setColor(0);
	setLargura(0);
	setAltura(0);
}//construtor
FiltroNegativo::FiltroNegativo(int largura, int altura, int color){
	setColor(color);
	setLargura(largura);
	setAltura(altura);
}//construtor
FiltroNegativo::~FiltroNegativo(){
} //destrutor

void FiltroNegativo::pixels(ofstream &imagem){
	int color = getColor();
	int largura = getLargura();
	int altura = getAltura();

	for(int l = 0; l < altura; l++){
		for(int c = 0; c < largura; c++){
			matrizR[l][c] = color - matrizR[l][c];
			imagem << matrizR[l][c];

            matrizG[l][c] = color - matrizG[l][c];
            imagem << matrizG[l][c];

            matrizB[l][c] = color - matrizB[l][c];
            imagem << matrizB[l][c];

		}
	}
}
