#include <iostream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include "filtropolarizado.h"
using namespace std;

FiltroPolarizado::FiltroPolarizado(){
	setColor(0);
	setLargura(0);
	setAltura(0);
}//construtor
FiltroPolarizado::FiltroPolarizado(int color, int largura, int altura){
	setColor(color);
	setLargura(largura);
	setAltura(altura);
}//construtor
FiltroPolarizado::~FiltroPolarizado(){
} // Método destrutor

void FiltroPolarizado::pixels(ofstream &imagem){
	int color = getColor();
	int largura = getLargura();;
	int altura = getAltura();

	for(int k = 0; k < altura; ++k){
    		for(int l = 0; l < largura; ++l){
           		if(matrizR[k][l] < color/2){
               			matrizR[k][l] = 0;
          		}
                else{
		               matrizR[k][l] = color;
          		}
           		if(matrizG[k][l] < color/2){
               			matrizG[k][l] = 0;
           		}
                else{
               			matrizG[k][l] = color;
           		}
           		if(matrizB[k][l] < color/2){
               			matrizG[k][l] = 0;
           		}
                else{
		               matrizB[k][l] = color;
          		}

                imagem << matrizR[k][l];
                imagem << matrizG[k][l];
                imagem << matrizB[k][l];
		}
	}
}

