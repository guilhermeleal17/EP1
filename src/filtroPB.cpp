#include <iostream>
#include <string>
#include <cstdio>
#include "filtroPB.h"

using namespace std;

FiltroPB::FiltroPB(){
	setColor(0);
	setLargura(0);
	setAltura(0);
}//construtor

FiltroPB::FiltroPB(int largura, int altura, int color){
	setColor(color);
	setLargura(largura);
	setAltura(altura);
}//construtor

FiltroPB::~FiltroPB(){
} // Método Destrutor


void FiltroPB::pixels(ofstream &imagem){
	int altura = getAltura();
	int largura = getLargura();
	int cinza;

	for(int i = 0; i < altura; i++){
        for(int j = 0; j < largura; j++){
            cinza = (0.299 * matrizR[i][j]) + (0.587 * matrizG[i][j]) + (0.144 * matrizB[i][j]);
           	matrizR[i][j] = cinza;
           	matrizG[i][j] = cinza;
           	matrizB[i][j] = cinza;

            imagem << matrizR[i][j];
            imagem << matrizG[i][j];
            imagem << matrizB[i][j];
        }
	}
}
