#ifndef FILTROPOLARIZADO_HPP
#define FILTROPOLARIZADO_HPP
#include <iostream>
#include "imagem.h"
class FiltroPolarizado: public Imagem{
public:
	FiltroPolarizado();
	~FiltroPolarizado();
	FiltroPolarizado(int color, int largura, int altura);

	void pixels(ofstream &imagem);
};
#endif
