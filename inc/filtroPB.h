#ifndef FILTROPB_HPP
#define FILTROPB_HPP
#include <iostream>
#include "imagem.h"
using namespace std;
class FiltroPB:public Imagem{
public:
    FiltroPB();
	~FiltroPB();
	FiltroPB(int largura, int altura, int color);
	void pixels(ofstream &imagem);
};
#endif
