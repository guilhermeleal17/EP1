#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <string>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
using namespace std;
class Imagem {
private:
	string magic_number;
	int color;
	int largura;
	int altura;

public:
	unsigned char **matrizR;
	unsigned char **matrizG;
	unsigned char **matrizB;

	ofstream imagem_saida;

	Imagem();
	~Imagem();
	Imagem(string magic_number, int largura, int altura, int color);

	string getMagic_number();
	void setMagic_number(string magic_number);
	int getColor();
	void setColor(int color);
	int getLargura();
	void setLargura(int largura);
	int getAltura();
	void setAltura(int altura);

	void AbrirImagem(ofstream &imagem);
	virtual void pixels(ofstream &imagem);
};
#endif
