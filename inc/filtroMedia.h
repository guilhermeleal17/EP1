#ifndef FILTROMEDIA_HPP
#define FILTROMEDIA_HPP
#include <iostream>
#include "imagem.h"

class FiltroMedia: public Imagem{
    public:
        FiltroMedia();
        virtual ~FiltroMedia();
        FiltroMedia(int largura, int altura, int color);

        void pixels(ofstream &imagem);

};
#endif // FILTROMEDIA_HPP
