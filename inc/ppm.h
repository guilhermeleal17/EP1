#ifndef PPM_HPP
#define PPM_HPP

#include "imagem.h"
#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <cstring>

using namespace std;

class PPM : public Imagem {
	public:
		//Construtor/Destrutor
		PPM();
		~PPM();
		void VerificaNumeroMagico();
};
#endif
