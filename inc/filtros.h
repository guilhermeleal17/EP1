#ifndef FILTROS_HPP
#define FILTROS_HPP


class filtros{
    public:
        filtros();
        virtual ~filtros();

    private:
        typedef struct {
            int red;
            int green;
            int blue;
        } pixel;
};
#endif // FILTROS_HPP
