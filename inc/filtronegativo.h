#ifndef FILTRONEGATIVO_HPP
#define FILTRONEGATIVO_HPP
#include <iostream>
#include "imagem.h"
class FiltroNegativo: public Imagem {
public:
	FiltroNegativo();
	~FiltroNegativo();
	FiltroNegativo(int largura, int altura, int color);
	void pixels(ofstream &imagem);
};
#endif
